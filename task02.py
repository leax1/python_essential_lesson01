class Feedback:
    """Відгук до книги"""

    def __init__(self, text):
        self.text = text

    def __str__(self):
        return self.text

    def __repr__(self):
        return self.text


class Book:
    """Клас, який описує книгу"""

    def __init__(self, author, title, year, genre):
        self.author = author
        self.title = title
        self.year = year
        self.genre = genre

    def __str__(self):
        feedbacks_str = ""

        if hasattr(self, "_feedbacks"):
            feedbacks_str = "\nВідгуки:\n" + "\n".join(map(str, self._feedbacks))

        return (
            f'{self.author} "{self.title}", {self.year} ({self.genre})'
            f"{feedbacks_str}"
        )

    def __repr__(self):
        return f"{self.author}-{self.title}-{self.year}-{self.genre}"

    def add_feedback(self, feedback_text: str):
        if hasattr(self, "_feedbacks"):
            self._feedbacks.append(Feedback(feedback_text))
        else:
            self._feedbacks = [Feedback(feedback_text)]


if __name__ == "__main__":
    book1 = Book(
        "Марк Твен", "Пригоди Тома Сойера та Гекельберрі Фіна", 1882, "пригоди"
    )
    book2 = Book("Дмитро Маханенко", "Шлях Шамана", 2012, "ЛітРПГ")
    book3 = Book("Сергій Зайцев", "ВІКС", 2010, "ЛітРПГ")

    book1.add_feedback("Чудова книжка!")
    book1.add_feedback("Не міг відірватися!")
    print(book1)
    print()

    book2.add_feedback("Я від неї шалію!")
    book2.add_feedback("Читав всю ніч до ранку!")
    print(book2)
    print()

    print(book3)
