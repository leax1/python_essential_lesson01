class Car:
    """Клас, що описує автомобіль"""

    def __init__(self, id_number: int, make: str, model: str, color: str, year: int):
        self.id_number = id_number
        self.make = make
        self.model = model
        self.color = color
        self.year = year

    def __str__(self):
        return f"id={self.id_number}: {self.make} {self.model} - {self.year} ({self.color})"

    def __repr__(self):
        return f'Car("{self.id_number},{self.make}","{self.model}","{self.color}",{self.year})'


class AutoShop:
    """Клас автосалону"""

    def __init__(self):
        self.__cars: [Car] = []

    def add_car(self, car: Car):
        self.__cars.append(car)

    def __str__(self):
        return "Auto shop:\n" + "\n".join(map(str, self.__cars))

    def sell_car(self, id_number):
        for car in self.__cars:
            if car.id_number == id_number:
                print("Продане авто: ", car)
                self.__cars.remove(car)
                break
        else:
            print(f"Error: Не знайдене авто id_number={id_number}")


if __name__ == "__main__":
    auto_shop = AutoShop()
    auto_shop.add_car(Car(1, "Citroen", "C3", "red", 2008))
    auto_shop.add_car(Car(2, "Audi", "A8", "silver", 2024))
    auto_shop.add_car(Car(3, "Reno", "Logan", "green", 2007))

    print(auto_shop)

    auto_shop.sell_car(2)

    print(auto_shop)

    auto_shop.sell_car(10)

    print(auto_shop)
