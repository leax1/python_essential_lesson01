class Book:
    """Клас, який описує книгу"""

    def __init__(self, author, title, year, genre):
        self.author = author
        self.title = title
        self.year = year
        self.genre = genre

    def __str__(self):
        return f'{self.author} "{self.title}", {self.year} ({self.genre})'

    def __repr__(self):
        return f"{self.author}-{self.title}-{self.year}-{self.genre}"


if __name__ == "__main__":
    book1 = Book(
        "Марк Твен", "Пригоди Тома Сойера та Гекельберрі Фіна", 1882, "пригоди"
    )
    book2 = Book("Дмитро Маханенко", "Шлях Шамана", 2012, "ЛітРПГ")
    book3 = Book("Сергій Зайцев", "ВІКС", 2010, "ЛітРПГ")

    print(book1, "\n", book2, "\n", book3)
    print([book1, book2, book3])
